import React from 'react';
import { Line } from 'react-chartjs-2';

class NameForm extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            value: '',
            selectedTimeUnit: 's',
            n: 0,
            showResult: false,
            halfLife: 14.959,
            originN: 650,
            t: 8,
            lambda: 0,
            showedTab: 'graph',
            nDatasheet:[],
            nData : [],
            timeData: []
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleTimeUnitChange = this.handleTimeUnitChange.bind(this);
        this.calculateRadioactiveDecay = this.calculateRadioactiveDecay.bind(this);

        this.onChangeHalfLife = this.onChangeHalfLife.bind(this);
        this.onChangeTime = this.onChangeTime.bind(this);
        this.onChangeOriginN = this.onChangeOriginN.bind(this);
        this.showTab = this.showTab.bind(this);
        this.calculateDataSheet  = this.calculateDataSheet.bind(this);
    }

    onChangeOriginN(event) {
        this.setState({
            originN: event.target.value
        });
    }

    onChangeTime(event) {
        this.setState({
            t: event.target.value
        });
    }

    onChangeHalfLife(event) {
        this.setState({
            halfLife: event.target.value
        });
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleTimeUnitChange(event) {
        this.setState({
            selectedTimeUnit: event.target.value
        })
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    showTab(tab) {
        this.setState({
            showedTab : tab
        });
    }

    calculateRadioactiveDecay() {

        if (this.state.originN === null || this.state.t === null || this.state.halfLife === 0) {
            alert("Please specify the input first");
        } else {

            var lambda = (Math.log(1 / 2)) / (-1 * this.state.halfLife);
            var n = this.state.originN * Math.exp(-1 * lambda * this.state.t);
            this.setState({
                n: n,
                showResult: true,
                lambda: lambda
            }, () => {
                this.calculateDataSheet();
            });
        }

       
    }

    calculateDataSheet(){
        var timeDataTemp = [0];
        var nDataTemp = [this.state.originN];

        var nDatasheetTemp = [{timeData: 0, nData: this.state.originN}];
        for(var i = 1; i <= this.state.t; i++){
            var nTemp = this.state.originN * Math.exp(-1 * this.state.lambda * i);
            nDatasheetTemp.push({timeData: i, nData: nTemp});
            timeDataTemp.push(i);
            nDataTemp.push(nTemp);
        }

        this.setState({
            nDatasheet : nDatasheetTemp,
            timeData : timeDataTemp,
            nData: nDataTemp
        
        }, ()=>{
            console.log(this.state.nDatasheet);
        })
    }
    
    render() {
        return (

            <div className="columns">
                <div className="column is-4">

                    <div className="field">
                        <label className="label">t<sub>1/2</sub> (Half-Life of Substance)</label>
                        <div className="control">

                            <div className="columns">
                                <div className="column is-8">
                                    <input className="input is-small k1-text" type="text" 
                                           value={this.state.halfLife} onChange={this.onChangeHalfLife} />
                                </div>
                                <div className="column is-3">
                                    <div className="select is-small is-fullwidth">
                                        <select onChange={this.handleTimeUnitChange} value={this.state.selectedTimeUnit}>
                                            <option value='s'>Second</option>
                                            <option value='h'>Hour</option>
                                            <option value='d'>Day</option>
                                            <option value='y'>Year</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>


                    <div className="field">
                        <label className="label">N<sub>0</sub> (Initial radiactive substance)</label>
                        <div className="control">

                            <div className="columns">
                                <div className="column is-8">
                                    <input className="input is-small k1-text" 
                                           type="text" 
                                           value={this.state.originN} 
                                           onChange={this.onChangeOriginN} />
                                </div>

                                <div className="column is-3">
                                    <div className="select is-small is-fullwidth">
                                        <select>
                                            <option value='bq'>Bq</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="field">
                        <label className="label">t (Time to be calculate)</label>
                        <div className="control">

                            <div className="columns">
                                <div className="column is-8">
                                    <input className="input is-small k1-text" 
                                           type="text" 
                                           value={this.state.t} onChange={this.onChangeTime} />
                                </div>
                                <div className="column is-3">
                                    <div className="select is-small is-fullwidth">
                                        <select onChange={this.handleTimeUnitChange} value={this.state.selectedTimeUnit}>
                                            <option value='s'>Second</option>
                                            <option value='h'>Hour</option>
                                            <option value='d'>Day</option>
                                            <option value='y'>Year</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <div className="field">
                        <button className="button is-primary is-fullwidth" onClick={this.calculateRadioactiveDecay}>
                            Calculate
                        </button>
                    </div>
                </div>

                <div className="column">
                    <div className="tabs">
                        <ul>
                            <li className={this.state.showedTab == 'result' ? 'is-active':''} onClick={() => this.showTab("result")}><a>Result</a></li>
                            <li className={this.state.showedTab == 'graph' ? 'is-active':''} onClick={() => this.showTab("graph")}><a>Graph</a></li>
                            <li className={this.state.showedTab == 'table' ? 'is-active':''} onClick={() => this.showTab("table")}><a>Table</a></li>
                        </ul>
                    </div>


                    {/* Div for Result tab */}
                    <div style={{display: this.state.showedTab == 'result' ? 'block' : 'none'}}>
                        <p className="is-size-1">
                            N =

                            <span style={{ display: this.state.showResult ? 'inline-block' : 'none' }}> {this.state.n} Bq</span>
                        </p>

                        <p className="is-size-7" style={{ marginTop: '20px' }}>
                            <i>General Solution</i><br />
                            <table>
                                <tbody>
                                <tr>
                                    <td>&lambda;</td> <td>&nbsp;=&nbsp;</td> <td>{this.state.lambda}</td>
                                </tr>
                                <tr>
                                    <td>N(t)</td> <td>&nbsp;=&nbsp;</td> <td>
                                        {this.state.originN} e<sup>-{this.state.lambda}t</sup>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </p>
                    </div>


                    {/* Div for Graph tab */}
                    <div style={{display: this.state.showedTab == 'graph' ? 'block' : 'none'}}>
                    <Line data={
                     {
                        labels: this.state.timeData,
                        datasets: [{
                        label: "Radionuclide Substance",
                        backgroundColor: '#0984e3',
                        borderColor: '#0984e3',
                        data: this.state.nData,
                        }]
                    }
                    }    
                    />
                    </div>


                    {/* Div for Chart tab */}
                    <div style={{display: this.state.showedTab == 'table' ? 'block' : 'none'}}>
                        <table className="table">
                            <thead>
                                <th>t</th><th>N (Radionuclide Substance)</th><th>Unit : (Bq)</th>
                            </thead>

                            <tbody>
                                {this.state.nDatasheet.map((data, index) => (
                                    <tr>
                                        <td>{data.timeData}</td>
                                        <td>{data.nData}</td>
                                        <td>Bq</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>


            // <form onSubmit={this.handleSubmit}>
            //   <label>
            //     Name:
            //     <input type="text" value={this.state.value} onChange={this.handleChange} />
            //   </label>
            //   <input type="submit" value="Submit" />
            //   <div>
            //       {this.state.value}
            //   </div>
            // </form>
        );
    }
}

export default NameForm;