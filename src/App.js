import React from 'react';
import './App.css';
import 'bulma/css/bulma.css';
import NameForm from './component/DecayCalculator';
function App() {
  return (

    <div>
      <section className="hero">
        <div className="hero-body">
          <div className="container">

            <div className="level">
              <div className="level-left">
                <img
                  src="radioactive.png"
                  width="80" 
                  alt="Atom"/>
              </div>

              <div className="level-left">
                <div>
                  <h1 className="title">
                    Radioactive-Decay Substance Calculator<br />

                  </h1>
                  <h4>Calculus 2 : Ordinary Differential Equation</h4>
                </div>
              </div>

              <div className="level-right">
                <p>
                  <a href="/" className="button is-default is-small">Download Manuscript (PDF)</a>
                </p>
                <a href="https://gitlab.com/gilangprambudi/radioactive-decay-calculator"
                  className="button is-default is-small">Access Code (GitLab)</a>
              </div>
            </div>



          </div>
        </div>
      </section>

      <section className="section">
        <div className="container-fluid">
          <div className="columns">
            <div className="column is-7">
              <p className="is-size-4 is-family-sans-serif has-text-primary has-text-weight-medium">
                Define value to Parameters</p>

              <p className="is-size-6">
               The equation has constants to be defined. Those are:<br/> 
                  <strong>t<sub>1/2</sub></strong>, <strong>N<sub>0</sub></strong> &amp; <strong>t</strong>

              </p>
            </div>

            <div className="column is-4">
              <img src="decay-equation.png" width="330" alt="Equation formula"/>
            </div>
          </div>

          <NameForm></NameForm>
        </div>
        
      </section>

      
    </div>
  );
}

export default App;
